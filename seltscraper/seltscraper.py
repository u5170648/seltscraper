"""
Class etc. associated with trying to extract SELT/SELS data from pdfs.

Lincoln Smith 2018
"""
import re
import subprocess
from itertools import chain, takewhile


class SELTScraper:
    results_block_header = r'Experience of learning'

    def __init__(self, fname):
        """
        Takes the pdf file pointer and sets up a list of strings so we can try and parse the pdf
        text.
        """
        self.fname = fname
        result = subprocess.run(['pdftotext', fname, '-'], stdout=subprocess.PIPE)
        raw_strings = result.stdout.decode('utf-8')

        assert raw_strings, 'PDF appears to have been empty'

        self.raw_strings = raw_strings.splitlines()
        self.strings = [s for s in self.raw_strings if s]
        self.course_code = ''
        self.course_description = ''
        self.session = ''
        self.year = ''
        self.set_course_and_session()

    def get_first_regex_match_index(self, lines, regex):
        """Return the index of the first item in the list of lines that matches the regex."""
        index_gen = (
                i for i, s in enumerate(lines) if re.match(regex, s)
                )
        try:
            return next(index_gen)
        except StopIteration:
            raise IndexError('Couldn\'t find an entry in the string list starting '
                             'with "{}"'.format(regex))

    def get_first_regex_match(self, lines, regex):
        """Return the first item in the list that matched the regex."""
        return self.strings[self.get_first_regex_match_index(lines, regex)]

    def set_course_and_session(self):
        """
        Get the course code, description, session, year, enrolments and responses for this course.
        """
        # Format of the header string is:
        # "<course_code> - <description string>, <session string> <year>"
        header = self.get_first_regex_match(self.strings, r'[A-Z]{4}\d{4}').split(',')
        self.course_code = header[0].split()[0]
        self.course_description = header[0][len(self.course_code):].lstrip(' -')
        self.session = ' '.join(header[1].split()[:-1])
        self.year = header[1].split()[-1]

        header = self.get_first_regex_match(self.strings, r'\d{1,3} Enrolments')
        match = re.match(r'(\d{1,3}) Enrolments; (\d{1,3}) Responses', header)
        self.enrolments = int(match.group(1))
        self.responses = int(match.group(2))

    def get_survey_questions(self):
        """Extract the survey questions from the pdf."""
        if hasattr(self, 'survey_questions'):
            return self.survey_questions

        self.survey_questions = []
        for i, s in enumerate(self.raw_strings):
            # Survey questions start with lines that look like '1. Lots of words', and all
            # subsequest lines until the next blank link belong to the same question unless you're
            # question 3 in which case you just cosy up to the next question to make things
            # difficult
            if re.match(r'\d\. \w+', s):
                self.survey_questions.append(' '.join(chain(
                    self.raw_strings[i:i+1],
                    takewhile(lambda x: x and not re.match(r'\d\. \w+', x), self.raw_strings[i+1:])
                    )))
        return self.survey_questions

    def get_results_block(self):
        """Return all the strings after a header string."""
        # The header for the survey results block changes a little sometimes...
        return self.strings[
                self.get_first_regex_match_index(self.strings, self.results_block_header):]

    def get_question_responses(self):
        """
        Get the percentage of respondents who selected a particular response for each
        question.
        """
        if hasattr(self, 'question_responses'):
            return self.question_responses

        # Response percentages appear in order, five lines at a time with rubbish in between.
        strings = self.get_results_block()
        self.question_responses = {}
        for q_num in range(1, len(self.get_survey_questions()) + 1):
            for i, s in enumerate(strings):
                if re.match(r'\d{1,3}\.?\d?%$', s):
                    matches = list(
                            takewhile(lambda x: re.match(r'\d{1,3}\.?\d?%$', x), strings[i:])
                            )
                    if len(matches) == 5:
                        self.question_responses[q_num] = matches
                        strings = strings[i+len(matches):]
                        break

        assert len(self.survey_questions) == len(self.question_responses), (
            'Failed to find matching sets of responses for all survey questions.'
            )
        return self.question_responses

    def get_question_stats(self):
        """Extract the stats analysis for the responses to each question."""
        if hasattr(self, 'question_stats'):
            return self.question_stats

        # Response stats look like a sequence of lines that match:
        # r'(n|av\.|dev\.|ab\.)=\d{1,3}\.?\d?$'
        strings = self.get_results_block()
        question_stats = {}
        for q_num in range(1, len(self.get_survey_questions()) + 1):
            for i, s in enumerate(strings):
                # Find an entry that indicates the start of a stats block, stuff the results in a
                # dict
                if re.match(r'n=\d+$', s):
                    question_stats[q_num] = {'n': None, 'av.': None, 'dev.': None, 'ab.': None}
                    question_stats[q_num]['n'] = int(re.search(r'\d+$', s).group())
                    # There can be up to three more lines of stats
                    for s2 in strings[i+1:i+4]:
                        if re.match(r'av\.=', s2):
                            question_stats[q_num]['av.'] = float(
                                    re.search(r'\d\.?\d?$', s2).group()
                                    )
                            continue
                        if re.match(r'dev\.=', s2):
                            question_stats[q_num]['dev.'] = float(
                                    re.search(r'\d\.?\d?$', s2).group()
                                    )
                            continue
                        if re.match(r'ab\.=', s2):
                            question_stats[q_num]['ab.'] = int(re.search(r'\d+$', s2).group())

                    # Re-slice our collection of strings to eliminate what we've already processed,
                    # allowing for the variable number of matched strings
                    strings = strings[
                            i+len([q for q in question_stats[q_num].values() if q is not None]):
                            ]
                    break

        assert len(self.survey_questions) == len(question_stats), (
            'Failed to find matching sets of response stats for all survey questions.'
            )
        self.question_stats = question_stats
        return self.question_stats

    def gen_raw_responses(self):
        """Recreate the raw response scores from total responses and response percentages."""
        if hasattr(self, 'raw_responses'):
            return self.raw_responses

        responses = self.get_question_responses()
        stats = self.get_question_stats()
        raw_responses = {}

        for q_num in range(1, len(self.get_survey_questions()) + 1):
            num_responses = int(stats[q_num]['n'])
            raw_scores = []
            for percentage in responses[q_num]:
                percentage = float(re.match(r'(.+)%$', percentage).group(1))
                raw_scores.append(round(num_responses * percentage / 100.0))
            assert num_responses == sum(raw_scores), (
                    'Sum of raw scores doesn\'t match total survey responses for question '
                    '{}.'.format(q_num)
                    )
            raw_responses[q_num] = raw_scores

        assert len(self.get_survey_questions()) == len(raw_responses), (
                'Number of raw response dictionaries doesn\'t match number of survey questions.'
                )
        self.raw_responses = raw_responses
        return self.raw_responses
