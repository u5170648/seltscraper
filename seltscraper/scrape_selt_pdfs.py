#!/usr/bin/env python

import argparse
import glob
import sys
from itertools import chain
from seltscraper import SELTScraper


def scraper_to_csv(scraper):
    """Take a scraper and return a line formatted for output into the csv file."""
    line = [scraper.year, scraper.session, scraper.course_code, scraper.course_description,
            str(scraper.enrolments), str(scraper.responses)]
    for q_num, q in enumerate(scraper.get_survey_questions(), start=1):
        line = chain(
                line,
                ['"{}"'.format(q)],
                [str(i) for i in scraper.gen_raw_responses()[q_num]],
                scraper.get_question_responses()[q_num],
                [
                    str(scraper.get_question_stats()[q_num]['n']),
                    str(scraper.get_question_stats()[q_num]['av.']),
                    str(scraper.get_question_stats()[q_num]['dev.']),
                    str(scraper.get_question_stats()[q_num]['ab.'] or '0'),
                    ]
                )
    return ','.join(line)


def generate_csv(infiles, outfile):
    # Get the first pdf and work out how many questions we have.
    scraper = SELTScraper(infiles[0])
    num_qs = len(scraper.get_survey_questions())
    header = 'Year,Session,Code,Description,Enrolments,Responses,'
    for i in range(1, num_qs + 1):
        qs = 'Q{}'.format(i)
        q_string = ('{0},{0} Raw1,{0} Raw2,{0} Raw3,{0} Raw4,{0} Raw5,{0} P1,{0} P2,{0} P3,'
                    '{0} P4,{0} P5,{0} N,{0} Avg,{0} Std Dev,{0} Abstention,'.format(qs))
        header = header + q_string

    outfile.write(header + '\n')

    for fname in infiles:
        scraper = SELTScraper(fname)
        outfile.write(scraper_to_csv(scraper) + '\n')
    outfile.close()


def get_in_files(args):
    """Work out the list of files we will attempt to scrape."""
    files = set(args.files)
    for directory in args.batch_dir:
        directory = directory + '/*.pdf'
        files.update(set(glob.glob(directory)))
    files = list(files)
    files.sort()
    return files


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse SELT PDFs into csv file')
    parser.add_argument(
            'files', type=str, default=None, nargs='*',
            help='Files to process.'
            )
    parser.add_argument(
            '-o', '--outfile', type=str, default=None,
            help='Output file, defalts to stdout.'
            )
    parser.add_argument(
            '-b', '--batch_dir', type=str, default=[], action='append',
            help='Directory containing files to batch process.'
            )
    args = parser.parse_args()

    in_files = get_in_files(args)

    if in_files:
        if args.outfile is None:
            outfile = sys.stdout
        else:
            outfile = open(args.outfile, 'w')

        generate_csv(in_files, outfile)
