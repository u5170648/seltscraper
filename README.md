Small python module and command line script to extract data from SELT/SELS
pdfs.

Only tested on 2017 CECS results but should work with all existing files with a
little tweaking.

### Requirements
* Python 3.5+
* pdftotext (from poppler-utils)

### pdftotext versions
There appear to be two versions of pdftext kicking about: one from the poppler project, and one from the xpdf(reader) project. I used the one from the poppler project as it is packaged for most linux distros. The version from the xpdf project does a better job of grouping lines, and supports Windows so I might switch to that.
