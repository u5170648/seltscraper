"""
Test the operation of the SELT scraper.

Because the survey results are not publicly accessible you'll need to populate the `selt_pdfs`
directory yourself.
"""
import glob
import re
import unittest
from seltscraper.seltscraper import SELTScraper


class TestSELTScraper(unittest.TestCase):
    """Check parsing on a set of sample files."""
    test_files = [
            'test/selt_pdfs/COMP8260_2730_LRN_Web.pdf',
            'test/selt_pdfs/COMP8173_2750_LRN_Web.pdf',
            'test/selt_pdfs/COMP1030_2730_LRN_Web.pdf',
            ]

    @classmethod
    def setUpClass(cls):
        """Get the test files for the test suite."""
        cls.scrapers = [SELTScraper(f) for f in cls.test_files]

    def test_get_results_block(self):
        """Finds the string that delimits the start of the survey results."""
        self.assertTrue(self.scrapers[0].get_results_block())
        self.assertTrue(self.scrapers[2].get_results_block())

    def test_set_course_and_session(self):
        """Extracts course code, description, session, year correctly."""
        scraper = self.scrapers[0]
        self.assertEqual('COMP8260', scraper.course_code)
        self.assertEqual('Prof Prac 2', scraper.course_description)
        self.assertEqual('Sem 1', scraper.session)
        self.assertEqual('2017', scraper.year)
        self.assertEqual(57, scraper.enrolments)
        self.assertEqual(23, scraper.responses)
        scraper = self.scrapers[1]
        self.assertEqual('Software Engineering Processes', scraper.course_description)
        self.assertEqual('Wint.', scraper.session)

    def test_get_survey_question(self):
        """Extracts all the survey questions from the 'Experience of learning' block."""
        scraper = self.scrapers[0]
        self.assertListEqual(
                ['1. I had a clear idea of what was expected of me in this course',
                 '2. The teaching and learning activities (eg. lectures, tutorials, field trips) '
                 'supported my learning',
                 '3. I had ready access to the learning opportunities provided in this course '
                 '(eg. course notes, online materials, library resources, field trips)',
                 '4. The assessment seemed appropriate given the goals of the course',
                 '5. The feedback I received during the course supported my learning',
                 '6. Overall, I was satisfied with my learning experience in this course'
                 ],
                scraper.get_survey_questions()
                )

    def test_get_question_responses(self):
        """Extracts the correct reponse percentages for each survey question."""
        scraper = self.scrapers[0]
        self.assertDictEqual(
                {
                    1: ['0%', '4.3%', '13%', '60.9%', '21.7%'],
                    2: ['0%', '0%', '8.7%', '65.2%', '26.1%'],
                    3: ['0%', '0%', '9.1%', '54.5%', '36.4%'],
                    4: ['0%', '4.3%', '17.4%', '47.8%', '30.4%'],
                    5: ['0%', '4.3%', '26.1%', '39.1%', '30.4%'],
                    6: ['0%', '0%', '17.4%', '52.2%', '30.4%']
                    },
                scraper.get_question_responses()
                )

    def test_get_question_stats(self):
        """Extracts the stats block for each question."""
        scraper = self.scrapers[0]
        self.assertDictEqual(
                {
                    1: {'n': 23, 'av.': 4, 'dev.': 0.7, 'ab.': None},
                    2: {'n': 23, 'av.': 4.2, 'dev.': 0.6, 'ab.': None},
                    3: {'n': 22, 'av.': 4.3, 'dev.': 0.6, 'ab.': 1},
                    4: {'n': 23, 'av.': 4, 'dev.': 0.8, 'ab.': None},
                    5: {'n': 23, 'av.': 4, 'dev.': 0.9, 'ab.': None},
                    6: {'n': 23, 'av.': 4.1, 'dev.': 0.7, 'ab.': None},
                    },
                scraper.get_question_stats()
                )

    def test_gen_raw_responses(self):
        """Rounding gives us the raw values we'd expect."""
        scraper = self.scrapers[0]
        self.assertDictEqual(
                {
                    1: [0, 1, 3, 14, 5],
                    2: [0, 0, 2, 15, 6],
                    3: [0, 0, 2, 12, 8],
                    4: [0, 1, 4, 11, 7],
                    5: [0, 1, 6, 9, 7],
                    6: [0, 0, 4, 12, 7],
                    },
                scraper.gen_raw_responses()
                )


class TestBulkSELTScraper(unittest.TestCase):
    """Test across a directory of pdfs to quickly check for format changes."""

    @classmethod
    def setUpClass(cls):
        """Get the test files for the test suite."""
        fnames = glob.glob('test/selt_pdfs/*.pdf')
        cls.scrapers = [SELTScraper(f) for f in fnames]

    def test_set_course_and_session(self):
        """Extracts course code, description, session, year correctly."""
        for scraper in self.scrapers:
            with self.subTest(fname=scraper.fname):
                self.assertTrue(re.match(r'[A-Z]{4}\d{4}', scraper.course_code))
                self.assertTrue(re.match(r'(\w+\s?)+', scraper.course_description))
                self.assertTrue(re.match(r'\w+\.?( \d)?$', scraper.session))
                self.assertTrue(re.match(r'\d{4}$', scraper.year))
                self.assertTrue(scraper.enrolments)
                self.assertTrue(scraper.responses)

    def test_get_survey_question(self):
        """Check we got the right number, can't really do much about checking format."""
        for scraper in self.scrapers:
            with self.subTest(fname=scraper.fname):
                self.assertEqual(6, len(scraper.get_survey_questions()))

    def test_get_question_responses(self):
        """We extract five response values with the right format for each question."""
        for scraper in self.scrapers:
            with self.subTest(fname=scraper.fname):
                # Verify that all the lines we grabbed are correctly formatted.
                for q_num, responses in scraper.get_question_responses().items():
                    for value in responses:
                        with self.subTest(question_number=q_num, response_string=value):
                            self.assertTrue(re.match(r'\d{1,3}\.?\d?%$', value))

    def test_get_question_stats(self):
        """Extract the stats for each question."""
        for scraper in self.scrapers:
            with self.subTest(fname=scraper.fname):
                for q_num, stats in scraper.get_question_stats().items():
                    with self.subTest(question_number=q_num, stats=stats):
                        self.assertIsNotNone(stats['n'])
                        self.assertIsNotNone(stats['av.'])
                        self.assertIsNotNone(stats['dev.'])
                        # Abstentions should be either None or something that evaluates to True
                        self.assertTrue(stats['ab.'] is None or stats['ab.'])

    def test_gen_raw_responses(self):
        """This one pretty much works or not, so just run it..."""
        for scraper in self.scrapers:
            with self.subTest(fname=scraper.fname):
                scraper.gen_raw_responses()
